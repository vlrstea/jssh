<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<style>
		
		#smallImg{
			
			width: 20%;

		}

		#largeImg{

			height: auto;
			max-width: 100%;
		}

		#largeImgPanel{
			
			text-align: center;
			visibility: hidden;
			position: fixed;
			z-index = 100;
			top: 0;left: 0;
			width: 100%;
			height: 100%;
			background-color: rgba(100, 100, 100, 0.3);

		}
		
	</style>

	<script>
		
		var image = ['img/1.jpg', 'img/2.jpg', 'img/3.jpg', 'img/4.jpg', 'img/5.jpg', 'img/6.jpeg', 'img/7.jpeg'];

		var imageNumber = 0;
		var imageLength = image.length - 1;


		function changeImage(num){

			imageNumber = imageNumber + num;

			if(imageNumber < 0){

				imageNumber = imageLength;

			}

			if(imageNumber > imageLength){

				imageNumber = 0;

			}

			slideshow.src = image[imageNumber];

			return false;
		}

		function showImage(imgNumber){

			document.getElementById('largeImg').src = imgNumber;
			showLargeImage();

		}

		function showLargeImage(){

			largeImgPanel.style.visibility = 'visible';

			clearInterval(start);
		}



		function hideMe(obj){

			largeImgPanel.style.visibility = 'hidden';

			startInterval();
		}


		function startInterval(){

			start = setInterval('changeImage(1)', 5000);

		}


		document.onkeydown = function(){

			if(window.event.keyCode == 37){

				changeImage(-1);

			}

			if(window.event.keyCode == 39){

				changeImage(1);
				
			}

			return false;
		}

		startInterval();


	</script>


	<div class="aaa">
		
		<img id="smallImg" src="img/1.jpg" onclick="showImage(image[imageNumber])" name="slideshow" />
	<div>
		<!-- <img id="smallImg" src="img/2.jpg" onclick="showImage(image[imageNumber])" name="slideshow" /></div> -->

	
		<div class="bigImage">
			
		<div id="largeImgPanel" onclick="hideMe(this)">
			
			<img src="" id="largeImg" style="height:100%; margin:0;padding:0" />

		</div>

		</div>

		
		<div class="buttons">
				
				<a href="javascript:changeImage(-1)">Previous</a>
				<a href="javascript:changeImage(1)">Next</a>


		</div>



	</div>
</body>
</html>